# Generate a QR Code with contact data (HTML + JS)

## Todo

- [ ] Improve the phone number input
- [ ] Rename to `html-attendance-tracker`... or even put it inside the Flutter project.
- [ ] Add a Faq page with the data protection and what the ticket is used for. (it will be scanned and the data will be on the phone and will be send per email as a PDF to the organization) No data is stored in the internet except for phone backups and the email account of the organizer (if the pdf is sent)
- [ ] Add a header (name of the location / event) in the pdf and in the html
  - Can we use the url and get parameters? `logo=../image/abc.png&title=Schlofftheater: Spring&date=3-30. Juni 2021`
- [ ] We might want to have a description and list of events (date / hour) with a registration where we store the name of the people.
- [ ] An (unlinked) admin page, where the org can generate the QR Code that leads to the page itself and create event urls (stored in localstorage)
- [ ] Rename: it's not php and it's not ticketing... contact collecting?
- [ ] <https://github.com/flozz/stone.js/>: make the js translatable

## Flutter

- <https://pub.dev/packages/qr_code_scanner>
- <https://pub.dev/packages/flutter_email_sender>
- <https://pub.dev/packages/pdf>
