# Attendance Tracker

In many places you're supposed of keeping track of the people attending an event.

_Attendance Tracker_ consist of

- A small static website (`index.html` and `admin.html`) that let the attendees generate a QR Code containing the contact data.  
  With this Form, the audience can create (in advance or at the location) a PDF file that they can show on their phone or print on paper.
- An Android (and iOS) App that reads the QR Code and produces a List of the attendees as a PDF.

The contact data is only stored in the QR Code itself and on the Mobile Defice used for the registration.  
No data is sent to third parties or stored on a server.

## Usage

### Creating the personal QR Code

The attendee goes to the HTML page and fills the contact data...

<p><img src="docs/index-form.png" width="400px"></p>

<p><img src="docs/index-filled.png" width="400px"></p>

... generates the QR Code...

<p><img src="docs/index-qr-code.png" width="400px"></p>


The QR Code can be shown as is for registration or stored on the device as a PDF, possibly printed on paper, and shown later when getting to the event:

<p><a href="docs/pdf-qr-code.pdf"><img src="docs/pdf-qr-code.png" width="500px"></a></p>

### Creating the Attendance List

At the event the QR Codes can be scanned using a smart phone.

The final result is a PDF that can be kept on the phone or downloaded to a PC.

<p><img src="docs/index-qr-code.png" width="400px"></p>

## Development

### Tutorials

- [Navigate to a new screen and back](https://flutter.dev/docs/cookbook/navigation/navigation-basics)
- [Using SQLite In Flutter](https://petercoding.com/flutter/2021/03/21/using-sqlite-in-flutter/)
- [Return data from a screen](https://flutter.dev/docs/cookbook/navigation/returning-data)

### Todo

- [ ] Improve the content of the PDF with the QR Code
      - Add a title on top
      - Add a sentence that describes  the content of the QR Code
      - Add a sentence that says that the attendee can print the PDF or keep it on the phone to show it at the event.
- [ ] Add the QR-Code reader
  - Pass the code having been read to the parent screen.
- [ ] Translate the app into german.
- [ ] Undo for delete? <https://blog.usejournal.com/implementing-swipe-to-delete-in-flutter-a742e041c5dd>
- [ ] Check the email address or the phone number:
  - <https://pub.dev/packages/flutter_sms>
  - <https://pub.dev/packages/flutter_email_sender>
  - <https://pub.dev/packages/flutter_mailer>
  - https://maneesha-erandi.medium.com/sending-emails-with-flutter-dd5e2c182fc
