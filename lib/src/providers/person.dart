import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../models/person.dart';


class PersonProvider {
  late Database db;

  Future<Database> initialize() async {
    db = await openDatabase(
        join(await getDatabasesPath(), 'attendance_tracker.db'),
        version: 1,
        onCreate: _createTables,
    );
    return db;
  }

  // TODO: probably unused...
  Future<PersonProvider> open() async {
    db = await openDatabase(
        join(await getDatabasesPath(), 'attendance_tracker.db'),
        version: 2,
        onCreate: _createTables,
        // onUpgrade: (Database db, int oldVersion, int newVersion) async {
        //   if (oldVersion < 2) {
        //     await db.execute("ALTER TABLE Person ADD timestamp DATETIME DEFAULT CURRENT_TIMESTAMP");
        //   }
        // }

    );
    return this;
  }

  void _createTables(Database db, int version) async {
    await db.execute('''
            create table Person (
              id integer primary key autoincrement,
              firstname text not null,
              lastname text not null,
              email text not null,
              phone text not null,
              timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
            )
          ''');
  }

  Future <void> insert(Person person) async {
    await db.insert('Person', person.toMap());
  }

  Future <List<Person>> list() async {
    final queryResult = await db.query(
      'Person'
    );
    return queryResult.map((e) => Person.fromMap(e)).toList();
  }

  Future <List<Person>> listByName() async {
    final queryResult = await db.query(
      'Person',
      orderBy: 'lastname, firstname',
    );
    return queryResult.map((e) => Person.fromMap(e)).toList();
  }

  Future<void> delete(int id) async {
    await db.delete(
      'Person',
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<void> deleteAll() async {
    await db.delete(
      'Person'
    );
  }

}