class Person {
  final int? id;
  final String firstname;
  final String lastname;
  final String email;
  final String phone;
  final DateTime? timestamp;

  Person({
    this.id,
    required this.firstname,
    required this.lastname,
    required this.email,
    required this.phone,
    this.timestamp
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'firstname': firstname,
      'lastname': lastname,
      'email': email,
      'phone': phone
    };
  }

  Person.fromMap(Map<String, dynamic> e)
      : id = e['id'],
        firstname = e['firstname'],
        lastname = e['lastname'],
        email = e['email'],
        phone = e['phone'],
        timestamp = DateTime.parse(e['timestamp']);

  @override
  String toString() {
    return 'Person(id: $id, firstname: $firstname, lastname: $lastname, email: $email, phone: $phone, timestamp: $timestamp)';
  }
}