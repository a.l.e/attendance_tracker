import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import 'drawer.dart';
import 'qr_code.dart';

import '../providers/person.dart';
import '../models/person.dart';

class AddPersonRoute extends StatefulWidget {
  @override
  _AddPersonState createState() => _AddPersonState();

/*
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
  */
}

class _AddPersonState extends State<AddPersonRoute> {
  final _formKey = GlobalKey<FormState>();
  final firstnameController = TextEditingController();
  final lastnameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();

  @override
  void dispose() {
    firstnameController.dispose();
    lastnameController.dispose();
    emailController.dispose();
    phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context); // tabbing through the fields

    return Scaffold(
        appBar: AppBar(
          title: Text('Add to the list'),
        ),
        drawer: AppDrawer(),
        body: SingleChildScrollView( // scroll when the keyboard pops up
            child: Form(
                key: _formKey,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: OutlinedButton.icon(
                            onPressed: () async {
                              final result = await Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => QRCodeRoute()),
                              );
                              if (result == null) {
                                print('result is null');
                                return;
                              }
                              final fields = result.split('\t');
                              if (fields.length != 4) {
                                print('${fields.length} fields');
                                return;
                              }
                              print('all ok');
                              firstnameController.text = fields[0];
                              lastnameController.text = fields[1];
                              emailController.text = fields[2];
                              phoneController.text = fields[3];
                              // ScaffoldMessenger.of(context)
                              //   ..removeCurrentSnackBar()
                              //   ..showSnackBar(SnackBar(content: Text('$result ${fields.length}')));
                            },
                            icon: const Icon(Icons.qr_code_scanner),
                            label: Text('Scan'),
                          )
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Firstname',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        controller: firstnameController,
                        textInputAction: TextInputAction.next,
                        onEditingComplete: () => node.nextFocus(),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Lastname',
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        controller: lastnameController,
                        textInputAction: TextInputAction.next,
                        onEditingComplete: () => node.nextFocus(),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Email',
                        ),
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          if ((value == null || value.isEmpty) && phoneController.text.isEmpty) {
                            return 'Please enter an email address or a phone number.';
                          }
                          return null;
                        },
                        controller: emailController,
                        textInputAction: TextInputAction.next,
                        onEditingComplete: () => node.nextFocus(),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Phone Number',
                          helperText: '077 123 45 66',
                        ),
                        keyboardType: TextInputType.phone,
                        autocorrect: false,
                        inputFormatters: [
                          MaskTextInputFormatter(mask: "### ### ## ##"),
                        ],
                        validator: (value) {
                          if ((value == null || value.isEmpty) && emailController.text.isEmpty) {
                            return 'Please enter an email address or a phone number.';
                          }
                          return null;
                        },
                        controller: phoneController,
                        textInputAction: TextInputAction.done,
                        onEditingComplete: () => node.nextFocus(),
                      ),
                      Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: ElevatedButton(
                            onPressed: () async {
                              if (_formKey.currentState!.validate()) {
                                await PersonProvider().open().then((p) async => await p.insert(Person(
                                    firstname: firstnameController.text,
                                    lastname: lastnameController.text,
                                    email: emailController.text,
                                    phone: phoneController.text
                                )));
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(SnackBar(content: Text('Added ${firstnameController.text} ${lastnameController.text}')));

                                firstnameController.text = '';
                                lastnameController.text = '';
                                emailController.text = '';
                                phoneController.text = '';

                                // Navigator.pop(context);
                              }
                            },
                            child: Text('Add'),
                          )
                      ),
                    ]
                )
            )
        )
    );
  }
}