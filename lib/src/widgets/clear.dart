import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../providers/person.dart';

class ClearRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        title: Text('Export'),
    ),
    body: Column(
    children: <Widget>[
    OutlinedButton.icon(
    onPressed: () async {
      var provider = PersonProvider();
      await provider.initialize();
      await provider.deleteAll();
      ScaffoldMessenger.of(context)
          .showSnackBar(
          SnackBar(content: Text('The list has been emptied.')));
      Navigator.pop(context);
    },
      icon: const Icon(Icons.delete_forever),
      label: Text('Delete all'),
    )
    ]
    )
    );
  }
}
