import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../providers/person.dart';
import '../models/person.dart';

class ListPersonsRoute extends StatefulWidget {
  @override
  _ListPersonsState createState() => _ListPersonsState();
}
class _ListPersonsState extends State<ListPersonsRoute> {
  late PersonProvider provider;

  @override
  void initState() {
    super.initState();
    provider = PersonProvider();
    provider.initialize().whenComplete(() async {setState(() {});});
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('The list'),
        ),
        // StreamBuilder when you want to get further updates,
        // FutureBuild when you just want to wait for the data to be available
        // https://petercoding.com/flutter/2021/03/21/using-sqlite-in-flutter/
        body:   FutureBuilder(
            future: provider.listByName(),
            builder: (BuildContext context, AsyncSnapshot<List<Person>> snapshot) {
              if (snapshot.hasData) {
                // check if list is empty... if it's the case just add a text
                return ListView.builder(
                    itemCount: snapshot.data?.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Dismissible(
                        direction: DismissDirection.endToStart,
                        background: Container(
                          color: Colors.deepOrange,
                          alignment: Alignment.centerRight,
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: Icon(Icons.delete_forever),
                        ),
                        key: ValueKey<int>(snapshot.data![index].id!),
                        onDismissed: (DismissDirection direction) async {
                          await this.provider.delete(snapshot.data![index].id!);
                          setState(() {
                            snapshot.data!.remove(snapshot.data![index]);
                          });
                        },
                        child: Card(
                            child: ListTile(
                              contentPadding: EdgeInsets.all(8.0),
                              title: Text('${snapshot.data![index].firstname} ${snapshot.data![index].lastname}'),
                              subtitle: Text('${snapshot.data![index].email} ${snapshot.data![index].phone}'),
                            )
                        ),
                      );
                    }
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }
        )
      /*
      body: ListView.builder(
        itemCount: items2.length,
        itemBuilder: (context, index) {
          final item = items[index];
          final item2 = items2[index];
          return Dismissible(
            key: Key(item),
            onDismissed: (direction) {
              setState(() {
                items.removeAt(index);
              });
              // ScaffoldMessenger.of(context)
              //     .showSnackBar(SnackBar(content: Text('$item Removed')));
            },
            child: ListTile(title: Text('${item2.firstname}')),
          );
        },
      )
       */
    );
  }
}