import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_file_dialog/flutter_file_dialog.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:intl/intl.dart';

import '../providers/person.dart';

class ExportRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Export'),
      ),
      body: Column(
        children: <Widget>[
          OutlinedButton.icon(
            onPressed: () async {
              var provider = PersonProvider();
              await provider.initialize();
              var list = await provider.list();
              final pdf = pw.Document();
              if (list.length == 0) {
                pdf.addPage(pw.Page(
                    pageFormat: PdfPageFormat.a4,
                    build: (pw.Context context) {
                      return
                        pw.Paragraph(text:'No entry found.');
                    })); // Page
              } else {
                pdf.addPage(pw.MultiPage(
                  pageFormat: PdfPageFormat.a4,
                  margin: pw.EdgeInsets.all(1 * PdfPageFormat.cm),
                  build: (context) {
                    return [
                      pw.Header(level:1, text: 'Attendance List'),
                      pw.Paragraph(text: DateFormat("dd.MM.yyyy").format(list.first.timestamp!) + ' - ' +
                        DateFormat("dd.MM.yyyy").format(list.last.timestamp!)),
                      pw.Table.fromTextArray(context: context, data: <List<String>>[
                        <String>['Name', 'Email', 'Phone Number'],
                        ...list.map(
                                (row) => ['${row.firstname} ${row.lastname}', row.email, row.phone])
                      ]),
                    ];
                }));
              }

              // path_provider does not seem to allow to save to a place that is
              // "visible to the user.
              // so: prompt the user to save the file!
              final params = SaveFileDialogParams(
                  data: await pdf.save(),
                  fileName: 'attendance-${DateFormat('yyyy-MM-dd').format(DateTime.now())}.pdf'
              );
              final filePath = await FlutterFileDialog.saveFile(params: params);

              if (filePath != null) {
                ScaffoldMessenger.of(context)
                    .showSnackBar(
                    SnackBar(content: Text('The PDF has been saved.')));
                Navigator.pop(context);
              }
            },
            icon: const Icon(Icons.picture_as_pdf),
            label: Text('Export as PDF'),
          )
        ]
      )
    );
  }
}

/*
class MyHomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
      ),
      body: Center(
          child: GridView.count(
            crossAxisCount: 2,
            children: [
              Center(
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AddPersonRoute()),
                    );
                  },
                  child: Text(
                    'Add',
                    style: Theme.of(context).textTheme.headline5,
                  ),
                ),
              ),
              Center(
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ListPersonsRoute()),
                    );
                  },
                  child: Text(
                    'List',
                    style: Theme.of(context).textTheme.headline5,
                  ),
                ),
              ),
              Center(
                child: ElevatedButton(
                  onPressed: () {  },
                  child: Text(
                    'Export',
                    style: Theme.of(context).textTheme.headline5,
                  ),
                ),
              ),
              Center(
                child: ElevatedButton(
                  onPressed: () {  },
                  child: Text(
                    'Clear',
                    style: Theme.of(context).textTheme.headline5,
                  ),
                ),
              ),
            ],
          )
      ),
    );
  }
}
*/
