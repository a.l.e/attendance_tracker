import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../widgets/persons_list.dart';
import '../widgets/export.dart';
import '../widgets/clear.dart';

class AppDrawer extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text('Attendance Tracker')
          ),
          ListTile(
            title: Text('List'),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ListPersonsRoute()),
              );
            },
          ),
          ListTile(
            title: Text('Export'),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ExportRoute()),
              );
            },
          ),
          ListTile(
            title: Text('Clear'),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ClearRoute()),
              );
            },
          ),
        ],
      )
    );
  }
}