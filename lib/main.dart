import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'src/widgets/add_person.dart';

void main() => runApp(AttendanceTrackerApp());


class AttendanceTrackerApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Attendance Tracker',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: MyHomePage(),
      home: AddPersonRoute(),
    );
  }
}


